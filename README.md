# metaphactory deployments with docker-compose

This repository has been deprecated.

All content has moved to https://github.com/metaphacts/metaphactory-docker-compose

For existing installations you may use the following commands to switch your local clone to the new location:

```
git remote set-url origin https://github.com/metaphacts/metaphactory-docker-compose.git
git fetch
```

In case of questions, do not hesitate to contact support@metaphacts.com